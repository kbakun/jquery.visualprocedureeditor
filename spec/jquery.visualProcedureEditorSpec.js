jasmine.getFixtures().fixturesPath = '/spec/javascripts/fixtures';
jasmine.getJSONFixtures().fixturesPath = '/spec/javascripts/fixtures/json';
describe('VisualProcedureEditor', function () {
     beforeEach(function() {
            
             loadFixtures('rootFixture.html');
            $("#testObject").visualProcedureEditor();
         });
    it('should generate dom infrastructure first and show view 1 and hide view 2', function () {
       
        
        expect($('#testObject')).toBe('div#testObject');
        expect($('#testObject')).toHaveClass('VisualProcedureEditor');
        expect($('#testObject')).toContain('.view1');
        expect($('#testObject > div.view1')).toContain("div.uploader");
        expect($('#testObject > div.view1')).toContain("div.dropFile");
        expect($('#testObject > div.view1')).toContain("button.selectFile");
        expect($('#testObject > div.view1')).toContain("button.showView2");
        
        expect($('#testObject')).toContain('.view2');
        expect($('#testObject > div.view2')).toContain('img.background');
        expect($('#testObject > div.view2')).toContain('button.save');
        expect($('#testObject > div.view2')).toContain('button.reset');
        expect($('#testObject > div.view2')).toContain('div.elements');
        expect($('#testObject > div.view2')).toContain("div.element[data='nip']");
        expect($('#testObject > div.view2')).toContain("div.element[data='name']");
    });
    it('shoud update default options',function(){
        //getJSONFixture
    })
    it('should show view 1 after init', function () {
       
        expect($('#testObject > div.view2')).toBeHidden();
        expect($('#testObject > div.view1')).not.toBeHidden();
        
   
    }); 
    describe('ShowView2 Button', function () {
        it('should fire click event ', function () {
           
           
        
      
        
        
            var spyEvent = spyOnEvent('button.showView2', 'click');
            $('button.showView2').click();
            expect('click').toHaveBeenTriggeredOn('button.showView2');
            expect(spyEvent).toHaveBeenTriggered();


        
        });
        it('should show view 2 after click', function () {

            $("img.background").attr("src","test.jpg");
            $("button.showView2").click();  
            expect($('#testObject > div.view2')).not.toBeHidden();
            expect($('#testObject > div.view1')).toBeHidden();
        
        });
    
        it('should apear only if background image is selected', function () {

            expect($('button.showView2')).toBeHidden();
            $("img.background").attr("src","test2.jpg");
        //expect($('button.showView2')).toBeHidden();
        });
        
        it('should generate elements at good positions', function () {
              loadFixtures('rootFixture.html');
              var data = getJSONFixture("defConf.json");
            $("#testObject").visualProcedureEditor(data);
             $('#testObject > div.view2').show();     
            expect($("div.element").length).toEqual(data.elements.length);
            $.each(data.elements,function(index,element){
                console.log($($("div.element")[index]));
                expect($($("div.element")[index]).position().top).toEqual(element.top);
                expect($($("div.element")[index]).position().left).toEqual(element.left);
            })
        });
    });
    describe('Save Button', function () {
         beforeEach(function() {
            
             loadFixtures('rootFixture.html');
            
         });
         
        it('should fire click event ', function () {
            
           $("#testObject").visualProcedureEditor();
            var spyEvent = spyOnEvent('button.save', 'click');
            $('button.save').click();
            expect('click').toHaveBeenTriggeredOn('button.save');
            expect(spyEvent).toHaveBeenTriggered();


        
        });
        it('should send to server data ', function () {
             var foo, ret;
             foo = { elements: [
            {
                "id":"nip1",
                "name":"Nip1"
            },

            {
                "id":"name1",
                "name":"Nazwa1"
            }
            ],
                "saveCalback":function(data){
                    ret =  "testData";
                }
            };
             $("#testObject").visualProcedureEditor(foo);
             spyOn(foo, 'saveCalback');
             $('button.save').click();
            // expect(foo.saveCalback).toHaveBeenCalled();
             expect(ret).toEqual("testData");
        });
    });
describe('Reset Button', function () {
    it('should fire click event ', function () {
        loadFixtures('rootFixture.html');
        $("#testObject").visualProcedureEditor();
        var spyEvent = spyOnEvent('button.reset', 'click');
        $('button.reset').click();
        expect('click').toHaveBeenTriggeredOn('button.reset');
        expect(spyEvent).toHaveBeenTriggered();


        
    });
    it('should show view 1 and clear img after click', function () {
        loadFixtures('rootFixture.html');
        $("#testObject").visualProcedureEditor();
        $("img.background").attr("src","test2.jpg");
        $("#testObject > div.view1 > div.uploader > div.title").text("100%");
        $('#testObject > div.view1').hide();
        $('#testObject > div.view2').show();     
        $("button.reset").click();  
        expect($('#testObject > div.view1')).not.toBeHidden();
        expect($('#testObject > div.view2')).toBeHidden();
        expect($("img.background")).toHaveAttr("src","");
        expect($("#testObject > div.view1 > div.uploader > div.title")).toHaveText("upuść plik tutaj lub");
        
    });
});

});