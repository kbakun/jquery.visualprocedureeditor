// jQuery.visualProcedureEditor v0.1.0
// 5/2013 by Krzysztof Bakun (bakun.biz)
// License http://creativecommons.org/licenses/by-nc/3.0/legalcode
// To use: $('#target-div').visualProcedureEditor(options)
// Will generate UI for load image for background and pozition element on it and save

(function ($) { 


    
    

  
    $.fn.visualProcedureEditor = function ( options) {
        var GUID = "";
        var defaults = {
            "elements": [
            {
                "id":"nip",
                "name":"Nip",
                "top":100,
                "left":120,
                "width":200,
                "height":40
            },

            {
                "id":"name",
                "name":"Nazwa",
                "top":150,
                "left":120,
                "width":200,
                "height":40
            }
            ,

            {
                "id":"street",
                "name":"Ulica",
                "top":250,
                "left":190,
                "width":200,
                "height":40
            }
            ],
            saveCalback:function(data){
                console.log("need to implement def save method",data);
            },
            resetCalback:function(){
                console.log("reset");
            },

            uploadImage:"upload.php",
            showSaveButton:true,
            showResetButton:true,
            initalState:1,
            backgroundImageSrc:""
        };
        //console.log(options,options);
        
        options = $.extend(defaults, options);
        //console.log("after extends",options,options);
        
        return this.each(function (index, obj) {
            generateGUID();
            generateHtml(obj,index);
            if(options.initalState!=2){
                resetState(obj);
            }
            
        });
        function generateGUID(){
            var d = new Date();
            GUID = "visualProcedureEditor_"+d.getTime()+"_"+d.getUTCMilliseconds();

        }
        function generateHtml(obj,index){
         
            
            $(obj).empty();
            $(obj).addClass("VisualProcedureEditor");
        
            //console.log(index,obj,options);
            $(obj).append("<div class='view1'><img class='background' /><div id='uploader_"+GUID+"' class='uploader'><div class='title'>upuść plik tutaj <br />lub</div><div id='dropFile_"+GUID+"' class='dropFile'></div><button  id='selectFile_"+GUID+"' class='selectFile'>wybierz plik z dysku</button></div><button class='showView2'>przejdz do edycji</button></div><div class='view2'><img class='background' /><div class='elements'></div><div class='buttons'></div></div>");
        
            if(options.backgroundImageSrc!=""){
                $(obj).find("img.background").attr("src",options.backgroundImageSrc);
            }
        
        
            $(obj).find("div.buttons").append("<button class='save'>Zapisz</button>");
            if(!options.showSaveButton){
                $(obj).find("button.save").css("visibility","hidden");
            }
        
            $(obj).find("div.buttons").append("<button class='reset'>Zresetuj</button>");
            if(!options.showResetButton){
                $(obj).find("button.reset").css("visibility","hidden");
            
            }
            $.each(options["elements"], function(index,element){
                //console.log(element);
                function def(val){
                    if(typeof val === 'undefined' || val === null){
                        return 0;
                    }
                    return val;
                }
                $(obj).find("div.elements").append("<div class='element' style='left:"+def(element["left"])+"px;top:"+def(element["top"])+"px;width:"+def(element["width"])+"px;height:"+def(element["height"])+"px; position: absolute;' data='"+element["id"]+"'><span class='bg'></span><span class='name'>"+element["name"]+"</span><span class='location'></span></div>");
            });
            $.each($(obj).find("div.element"),function(index,element){
                updateDescription(element);
            })
         
        
            function updateDescription(element){
                var target =  $(element);
                var pos =  target.position();
                target.find("span.location").text("pozyacja:("+pos.left+"x"+pos.top+") rozmiar:("+target.width()+"x"+target.height()+")");
            }
            $(obj).find("div.element").resizable({
                resize: function( event, ui ) {
                    //when resize
              
                    updateDescription(event.target);
                //$(event.target).find("span.location").text("pozyacja:("+pos.left+"x"+pos.top+") rozmiar:("+target.width()+"x"+target.height()+")");
                }
            }).draggable({
                scroll: true, 
                scrollSpeed: 50,
                scrollSensitivity: 100,
                drag: function( event, ui ) {
                    //when drag
                    updateDescription(event.target);
                }
            }).hover(
                function(){
                    $(this).siblings().hide();
                    
                }
                ,
                function(){
                    $(this).siblings().show();
                     
                }
            );
            ; 
            
            $(obj).attr("data",GUID);
            
            $(obj).find("button.showView2").unbind("click");
            $(obj).find("button.showView2").bind("click", function(){
                $(obj).find("div.view1").hide();
                $(obj).find("div.view2").show();
            });
            $(obj).find("img.background").unbind("load");
         
            $(obj).find("img.background").load(function(){
            
                refreshState(obj);
            });
        
            $(obj).find("button.save").unbind("click");
            $(obj).find("button.save").click(function(){
                var data = [];
                $(obj).find("div.element").each(function(index,element){
                    data.push({
                        "id":$(element).attr("data"),
                        "name":$(element).find("span.name").text(),
                        "top":$(element).position().top,
                        "left":$(element).position().left,
                        "width":$(element).width(),
                        "height":$(element).height()
                    }) ;
                
                
                });
             
                options["saveCalback"]({
                    "elements":data,
                    "img":{
                        "src":$(obj).find('.view2').find("img.background").attr("src"),
                        "width":$(obj).find('.view2').find("img.background").width(),
                        "height":$(obj).find('.view2').find("img.background").height()
                    }
                });
            
            });
         
            $(obj).find("button.reset").unbind("click");
            $(obj).find("button.reset").click(function(){
                resetState(obj)
            });
            
            var uploader = new plupload.Uploader({
                runtimes : 'gears,html5,flash,silverlight,browserplus',
                browse_button : 'selectFile_'+GUID,
                container : 'uploader_'+GUID,
                drop_element :'dropFile_'+GUID,
                max_file_size : '10mb',
                url : options.uploadImage,
                flash_swf_url : 'lib/plupload/plupload.flash.swf',
                silverlight_xap_url : 'libplupload/plupload.silverlight.xap',
                filters : [
                {
                    title : "Image files", 
                    extensions : "jpg,gif,png,pdf"
                }
			
                ]
            });
   
        
            uploader.init();
            uploader.bind('FilesAdded', function(up, files) {
                uploader.start();
		

                up.refresh(); // Reposition Flash/Silverlight
            });
            uploader.bind('UploadProgress', function(up, file) {
                $(obj).find("div.title").text(file.percent + "%");
            });
            uploader.bind('FileUploaded', function(up, file,response) {
                //console.log(response);
                var ret = $.parseJSON(response.response);
                //console.log(ret.location,$("img.background"));
                $(obj).find("img.background").attr("src",ret.location);  
                
            });
            if(options.initalState==2){
                $(obj).find("div.view2").show();
                $(obj).find("div.view1").hide();
            }
        }
        function resetState(obj){
            console.log("resetState 1");
            //options.resetCalback();
            console.log("resetState 2");
            $(obj).find("img.background").attr("src","");
            console.log("resetState 3");
            $(obj).find("div.view1 > div.uploader > div.title").html("upuść plik tutaj <br>lub");
            console.log("resetState 4");
            $(obj).find("div.view1").show();
            console.log("resetState 5");
            $(obj).find("div.view2").hide();
            console.log("resetState 6");
            refreshState(obj);
            console.log("resetState 7");
        }
        function refreshState(obj){
        
        
            if($(obj).find("img.background").attr("src")==""){
                $(obj).find("button.showView2").hide();
            }else{
                $(obj).find("button.showView2").show();
            }
        }     
      
    };
    

   
    
})(jQuery);